Chatting with Sourcing Staff
============================

All suppliers are required to register your company before accessing our system.

Please find out through following steps.

* :ref:`chat`.

.. _chat:

How can I chat with soucing staff ?
-----------------------------------

1. Go to `Home Page`_.

2. Login to your account.

3. Go to `User Profile Page`_. by clicking your name on the top right link. (left before "LOGOUT" link)

4. Click on "Chat" menu on the left menu

5. Choose the chat room you need to contact. Then, click "ENTER CHAT ROOM" to enter the chat room.

.. image:: /img/chat/select_chat_room.jpg
    :align: center

6. Type your message in Message box and press "SEND" button to send your message.

.. image:: /img/chat/send_message.jpg
    :align: center

7. You can attach file by clicking on the yellow button right to the "SEND" button. Then,
select your file to upload to the chat.

.. image:: /img/chat/send_file_button.jpg
    :align: center


.. _Home Page: http://pandorasourcing.com
.. _SUPPLIER REGISTRATION: http://pandorasourcing.com/en/supplier-registers
.. _User Profile Page: http://pandorasourcing.com/en/account
.. _EDIT: http://pandorasourcing.com/en/account/profile/edit
