.. role:: underline
    :class: underline

Frequently Asked Questions
==========================

You can look for your answer in following questions.

* :ref:`see-news`
* :ref:`forgot-password`
* :ref:`change-language`
* :ref:`contact-staff`

.. _see-news:

How can I see news from Pandora Sourcing ?
------------------------------------------

You can see our news from `News Page`_

.. _forgot-password:

How can I do if I forgot password ?
-----------------------------------

1. Go to `Login Page`_.

2. Click on "Forgot Password?" link below the "LOGIN" button.

3. Fill your email address that you need to recovery.

4. Click on "RESET PASSWORD" button to reset the password. A reset password email will be sent to your filled email address.

5. Open the reset password link in received email. e.g. http://pandorasourcing.com/accounts/password/reset/key/xxxxx

6. Fill the "New Password" and "New Password (again)" in Change Password Form to reset your password to your filled password.

.. _change-language:

How can I change language ?
---------------------------

1. Go to `Home Page`_.

2. Click at :underline:`EN` (For changing to English Language) or :underline:`TH` (For changing to Thai Language) link on the top right of the page.

.. _contact-staff:

How can contact sourcing staff ?
--------------------------------

You can get our contact information from `Contact Us Page`_

.. _Home Page: http://pandorasourcing.com
.. _Login Page: http://pandorasourcing.com/accounts/login/
.. _News Page: http://pandorasourcing.com/en/news/
.. _Contact Us Page: http://pandorasourcing.com/en/contact/