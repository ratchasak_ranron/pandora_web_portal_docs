Welcome to Pandora Sourcing Web Portal's Manual
===============================================

**Pandora Sourcing Web Portal** is a center application for communicating between PPT and all Suppliers.
Our system simplify supplier registration process for requesting required document as well as finding out
needed information such as Policy & Guideline.
We also provide more convenient way to contact with our sourcing staff via Chat Room.
Moreover, supplier can do an assessment questionnaires for further improvement.

The main documentation for the site is organized into a couple sections:

* :ref:`user-docs`
* :ref:`user-faq`

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Manual

   supplier_register
   chat
   policy
   questionnaire

.. _user-faq:

.. toctree::
   :maxdepth: 2
   :caption: Frequently Asked Questions (FAQ)

   faq
