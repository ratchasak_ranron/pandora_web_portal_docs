Policy & Guideline
==================

You can look for your answer in following questions.

* :ref:`get-payment-policy`

.. _get-payment-policy:

How can I download Payment Policy Documents ?
---------------------------------------------

1. Go to `Home Page`_.

2. Login to your account.

3. Go to `User Profile Page`_. by clicking your name on the top right link. (left before "LOGOUT" link)

4. Click to expand **"Policy & Guideline"** in the left menu and select "Payment Policy" menu.

5. There will be Payment Policy related documents for you to download. Then, click "Download" to download your needed document.

.. image:: /img/policy/download_policy.jpg
    :align: center

6. You can do the same steps for other policy.

.. _Home Page: http://pandorasourcing.com
.. _User Profile Page: http://pandorasourcing.com/en/account