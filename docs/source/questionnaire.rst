Assessing Questionnaire
=======================

For some suppliers are allowed to assess Pandora questionnaire for improving our service.
Allowed supplier will receive invite email to be assess the questionnaire.

You can look for your answer in following questions.

* :ref:`supplier-assessment`
* :ref:`pandora-assessment`

.. _supplier-assessment:

How can I assess questionnaire ?
--------------------------------

1. Go to `Home Page`_.

2. Login to your account.

3. Go to `User Profile Page`_. by clicking your name on the top right link. (left before "LOGOUT" link)

4. If you are able to do questionnaire "Questionnaires" menu will be appeared below the "Chat" menu. Click on the "Questionnaires" menu.

5. Click on the questionnaire link to do the questionnaire.

.. warning::
    Questionnaire will be available only within **Started date** and **Expired date**.
    So, please remind yourself to finish the questionnaire before hand.

.. image:: /img/questionnaire/select_questionnaire.jpg
    :align: center

6. In assessment page, there will be questions with 10 choices and a comment box. Select the choice and write down your comment for every questions.

.. image:: /img/questionnaire/do_questionnaire.jpg
    :align: center

7. Select **"SUBMIT QUESTIONNAIRE"** button to submit your answers.

8. You can see your assessed answers by select **"Supplier Performance Score"** from the left menu. In *Supplier Assesses Pandora* section, there will be a question you have assessed. Click on the list icon to view your answers.

.. image:: /img/questionnaire/see_assessed_questionnaire.jpg
    :align: center

.. _supplier-assessment:

How can I see Pandora assesses supplier result ?
--------------------------------

1. Go to `Home Page`_.

2. Login to your account.

3. Go to `User Profile Page`_. by clicking your name on the top right link. (left before "LOGOUT" link)

4. If you are able to see result "Supplier Performance Score" menu will be appeared. Then, click on the "Supplier Performance Score" menu.

5. In *Pandora Assesses Supplier* section, there will be a assessed result from pandora. Click on the link to download file.

.. image:: /img/questionnaire/pandora_assessed.jpg
    :align: center

.. _Home Page: http://pandorasourcing.com
.. _User Profile Page: http://pandorasourcing.com/en/account