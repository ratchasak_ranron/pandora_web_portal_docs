.. role:: underline
    :class: underline

Supplier Registration
=====================

All suppliers are required to register your company before accessing our system.

Please find out through following steps.

* :ref:`register`.
* :ref:`edit-profile`.
* :ref:`attach-file`.
* :ref:`approve-supplier`.

.. _register:

How can I register my company ?
-------------------------------

1. Go to `Home Page`_.

2. Click menu `SUPPLIER REGISTRATION`_ on the top menu.

.. image:: /img/supplier_register/click_supplier_register.jpg
    :align: center

3. Fill in account registration form and click on "JOIN TODAY" button.

.. image:: /img/supplier_register/fill_account_registration.jpg
    :align: center

4. Fill in company registration form and click on "REGISTER" button.

    :underline:`Field Description`

    Company Name
        **Required**. Company's name.
    TAX ID
        **Required**. Company's tax ID.
    Address
        **Required**. Company's address.
    City
        **Required**. Company's city.
    State
        **Required**. Company's state.
    Country
        **Required**. Company's country.
    Zip Code
        **Required**. Company's zip code.
    Phone
        **Required**. Company's phone.
    Fax
        **Optional**. Company's fax.
    Contact Person
        **Required**. Company's contact person.
    Email
        **Required**. Company's email.
    Mobile
        **Required**. Company's mobile phone no..
    Type
        **Required**. Pandora Type (can select more than one category).
    Core Business
        **Required**. Description about your company's core business.
    Capital Investment
        **Required**. Capital investment.
    Number of Employee
        **Required**. Number of employee in company.
    Year in business
        **Required**. Number of year in business.
    Sale Turnover per Year
        **Required**. Sale turnover per year.
    Owner Name
        **Required**. Company owner name.
    Company Certificate
        **Required**. Company certificate (can select more than one category).
    Other Certificate
        **Optional**. Other company certificate separated by comma, if you have any certificate other than in Company Certificate list.
    Pandora Category
        **Required**. Pandora supplier category (can select more than one category).

5. If you register successfully, there will be "SUPPLIER REGISTER SUCCESS". Then, click on "VIEW PROFILE SUPPLIER" button.

.. _edit-profile:

How can I review and update my company information ?
----------------------------------------------------

1. Go to `Home Page`_.

2. Login to your account.

3. Go to `User Profile Page`_. by clicking your name on the top right link. (left before "LOGOUT" link)

4. Click to expand "Company Profile" in the left menu and select "Profile" menu

5. In this page, you can review your submitted company information.

6. If you want to edit your company information, click "`EDIT`_" button on the bottom right of the page.

7. Edit Profile

    .. note::
        * You can click on your display photo to edit your profile picture.
        * You can click on "Update Account" link to edit your account name.

8. Click on "UPDATE" button in the bottom to update your company information.

.. _attach-file:

How can I submit required documents ?
-------------------------------------

1. Go to `Home Page`_.

2. Login to your account.

3. Go to `User Profile Page`_. by clicking your name on the top right link. (left before "LOGOUT" link)

4. Click to expand "Company Profile" in the left menu and select "Attach file" menu.

5. Select "SELECT FILE" button and select your document

    .. note:: **For Non-Disclosure Agreement (NDA):**
        You need to click on "Download format" link to download the NDA template.
        The downloaded file will be in Zip format (NDA.zip).
        You need to extract it and there will be 2 files in the folder.

        The template you choose is depend on your registering Pandora Category.

        * Use *"Non Disclosure agreement for Indirect (Machine,IT,Welfare and Production supply).doc"*
            If you are registering in :underline:`Machine,IT,Welfare and Production supply` category.
        * Use *"Non Disclosure agreement for Raw Materials (GEM, Metal, Non-Metal and Materials supply).doc"*
            If you are registering in :underline:`GEM, Metal, Non-Metal and Materials supply` category.

        After you signed the contract, upload the scanned signed document as same as other.

    .. note:: **For Code of Conduct (CoC):**
        You need to click on "Download format" link to download the CoC template (PDF).
        After you signed the contract, upload the scanned signed document as same as other.

    .. warning::
        * Allowed file extension: .pdf, .doc, .docx, .jpg, .png, .xlsx, .xls, .zip, .rar
        * Allowed file size: less than 10 MB

6. Click "SAVE" button to submit your documents.


.. _approve-supplier:

What can I do after submitted all required information ?
--------------------------------------------------------

You can do nothing, Just wait.

After you submitted all the information. *Pandora Sourcing Staff* will review your information.
If there is invalid information, you will be contacted, but if everything looks fine,
*Pandora Sourcing Staff* will approve your account to be an approved supplier.

As an approved supplier. you can do the following.

* Chat with *Pandora Sourcing Staff*
* Access Pandora document that you may need such as Payment Policy, Safety Policy, Delivery Policy and Import Policy.

.. _Home Page: http://pandorasourcing.com
.. _SUPPLIER REGISTRATION: http://pandorasourcing.com/en/supplier-registers
.. _User Profile Page: http://pandorasourcing.com/en/account
.. _EDIT: http://pandorasourcing.com/en/account/profile/edit
